# vergewalletinstall

A bash script to install the Verge Leviosa QT wallet and Verge Blockchain onto a computer running Ubuntu 18.04

To run installer download install.mag using 

1. git clone https://gitlab.com/bankonmeOS/vergewalletinstall.git
2. chmod + x install.mag
3. sudo ./install.mag

# https://tb-manual.torproject.org/secure-connections/
